﻿using System;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting.GenericClass
{
    public class EnemyManager : MonoBehaviour
    {
        private GenericEnemy<int> intEnemy;
        private GenericEnemy<bool> boolEnemy;

        private void Start()
        {
            intEnemy = new GenericEnemy<int>();
            boolEnemy = new GenericEnemy<bool>();
            intEnemy.SetHealth(100);
            boolEnemy.SetHealth(true);
        }

        private void Update()
        {
            Debug.Log(intEnemy.GetHealth());
            Debug.Log(boolEnemy.GetHealth());
        }
    }
}