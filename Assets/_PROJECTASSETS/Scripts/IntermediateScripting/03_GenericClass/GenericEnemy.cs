﻿namespace _PROJECTASSETS.Scripts.IntermediateScripting.GenericClass
{
    public class GenericEnemy<T>
    {
        private T health;
        public T GetHealth()
        {
            return health;
        }

        public void SetHealth(T newHealth)
        {
            health = newHealth;
        }
    }
}