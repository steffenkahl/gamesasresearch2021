﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting.Collections
{
    public class ListExample : MonoBehaviour
    {
        [SerializeField] private string name1;
        [SerializeField] private string name2;
        [SerializeField] private string name3;
        [SerializeField] private string name4;
        [SerializeField] private List<string> names;

        private void AddItemToList(string nameToAdd)
        {
            names.Add(nameToAdd);
        }

        private void RemoveItemFromList(string nameToRemove)
        {
            if (names.Contains(nameToRemove))
            {
                names.Remove(nameToRemove);
                Debug.Log("Removed Item: " + nameToRemove);
            }
        }

        private void Start()
        {
            AddItemToList(name1);
            AddItemToList(name2);
            AddItemToList(name3);
            AddItemToList(name4);

            //RemoveItemFromList(name3);
        }

        private void Update()
        {
            for (int i = 0; i < names.Count; i++)
            {
                Debug.Log("Current Name in List: " + names[i]);
            }
        }
    }
}