﻿using System;
using UnityEngine;
using System.Collections.Generic;
using System.Linq;

namespace PGaS
{
    public class DictionaryExample : MonoBehaviour
    {
        private Dictionary<string, int> myDictionary = new Dictionary<string, int>();

        [SerializeField] private string keyOne;
        [SerializeField] private string keyTwo;
        [SerializeField] private string keyThree;
        [SerializeField] private int valOne;
        [SerializeField] private int valTwo;
        [SerializeField] private int valThree;

        private void AddKeyAndValue(string key, int value)
        {
            if (!myDictionary.ContainsKey(key))
            {
                Debug.Log("Adding Key " + key + " and Value " + value);
                myDictionary.Add(key, value);
            }
        }

        private void RemoveKey(string key)
        {
            if (myDictionary.ContainsKey(key))
            {
                Debug.Log("Removing Key " + key);
                myDictionary.Remove(key);
            }
        }

        private void DirectAccessToSingleItemFromDictionary(string keyToAccess)
        {
            if (myDictionary.ContainsKey(keyToAccess))
            {
                Debug.Log("Accessing key: " + keyToAccess + "/ Value of Key is: " + myDictionary[keyToAccess]);
            }
        }

        private void CycleThroughDictionary()
        {
            if (myDictionary.Count <= 0)
            {
                Debug.Log("Dictionary has no keys");
                return;
            }

            for (int i = 0; i < myDictionary.Count; i++)
            {
                Debug.Log("FORLOOP / Key: " + myDictionary.Keys.ElementAt(i) + " with value: " + myDictionary.Values.ElementAt(i));
            }

            foreach (KeyValuePair<string, int> element in myDictionary) //schneller und schöner
            {
                Debug.Log("FOREACH / Key: " + element.Key + " with Value. " + element.Value);
            }
        }

        private void lookForAKey(string key)
        {
            int val;
            if (myDictionary.TryGetValue(key, out val))
            {
                Debug.Log("Key: " + key + " has this value: " + val);
            }
            else
            {
                Debug.Log("Hm, this key does not seem to exist (yet)!");
            }
        }

        private void Start()
        {
            AddKeyAndValue(keyOne, valOne);
            AddKeyAndValue(keyTwo, valTwo);
            AddKeyAndValue(keyThree, valThree);
            
            lookForAKey(keyTwo);
            
            RemoveKey(keyTwo);
            
            lookForAKey(keyTwo);
        }

        private void Update()
        {
            CycleThroughDictionary();
        }
    }
}