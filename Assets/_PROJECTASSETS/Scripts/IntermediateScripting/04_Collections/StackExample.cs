﻿using System;
using System.Collections;
using _PROJECTASSETS.Scripts.IntermediateScripting.GenericClass;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting.Collections
{
    public class StackExample : MonoBehaviour
    {
        private Stack exampleStack = new Stack();

        [SerializeField] private string name;
        [SerializeField] private int number;
        [SerializeField] private string otherName;
        [SerializeField] private int otherNumber;

        private void AddStringObjectToStack(string stringObject)
        {
            exampleStack.Push(stringObject);
        }
        private void AddIntObjectToStack(int intObject)
        {
            exampleStack.Push(intObject);
        }

        private void RemoveObjectFromStack()
        {
            if (exampleStack.Count > 0)
            {
                object item = exampleStack.Peek();
                Debug.Log("Removing Item: " + item);
                exampleStack.Pop();
            }
        }
        
        private void GetTopObjectFromStack()
        {
            if (exampleStack.Count > 0)
            {
                object item = exampleStack.Peek();
                Debug.Log("Top Item: " + item);
            }
        }


        private void Start()
        {
            AddStringObjectToStack(name);
            AddIntObjectToStack(number);
            AddIntObjectToStack(otherNumber);
            AddStringObjectToStack(otherName);
            
            RemoveObjectFromStack();
        }

        private void Update()
        {
            foreach (object item in exampleStack)
            {
                Debug.Log("Current Stack Object: " + item);
            }
        }
    }
}