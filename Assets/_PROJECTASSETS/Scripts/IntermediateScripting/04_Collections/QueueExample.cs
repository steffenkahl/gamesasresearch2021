﻿using System.Collections;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting.Collections
{
    public class QueueExample : MonoBehaviour
    {
        private Queue exampleQueue = new Queue();
        [SerializeField] private string name;
        [SerializeField] private string otherName;
        [SerializeField] private int number;
        [SerializeField] private int otherNumber;

        private void AddStringObjectToQueue(string stringObject)
        {
            exampleQueue.Enqueue(stringObject);
        }

        private void AddIntObjectToQueue(int intObject)
        {
            exampleQueue.Enqueue(intObject);
        }

        private void RemoveObjectFromQueue()
        {
            if (exampleQueue.Count > 0)
            {
                object item = exampleQueue.Peek();
                Debug.Log(message: "Removing item: " + item);
                exampleQueue.Dequeue();
            }
        }

        private void GetFirstObjectFromStack()
        {
            if (exampleQueue.Count > 0)
            {
                object item = exampleQueue.Peek();
                Debug.Log(message: "Next item: " + item);
            }
        }

        private void Start()
        {
            AddStringObjectToQueue(name);
            AddIntObjectToQueue(number);
            AddStringObjectToQueue(otherName);
            AddIntObjectToQueue(otherNumber);
            RemoveObjectFromQueue();
        }

        private void Update()
        {
            foreach (object item in exampleQueue)
            {
                Debug.Log(message: "Current Queue Object: " + item);
            }
        }
    }
}