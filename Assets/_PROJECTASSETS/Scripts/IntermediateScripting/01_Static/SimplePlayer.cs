﻿using UnityEngine;
using UnityEditor;

namespace PGaS
{
    public class SimplePlayer : MonoBehaviour
    {
        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                GameManager.QuitGame();
            }
        }
    }
}