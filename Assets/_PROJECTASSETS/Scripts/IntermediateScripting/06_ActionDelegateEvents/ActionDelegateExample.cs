﻿using System;
using System.Collections;
using UnityEngine;

namespace PGaS
{
    public class ActionDelegateExample : MonoBehaviour
    {
        private Action logAction;
        private Action<int> healthLogAction;
        private Action<string, int> betterHealthLogAction;
        
        private void SimpleLog()
        {
            Debug.Log("This is a simple log");
            
        }

        private void HealthLog(int playerHealth)
        {
            Debug.Log("Player Health: " + playerHealth);
        }

        private void BetterHealthLog(string playerName, int playerHealth)
        {
            Debug.Log("Player Name: " + playerName + "Player Health: " + playerHealth);
        }

        private void Start()
        {
            logAction = SimpleLog;
            logAction();

            healthLogAction = HealthLog;
            healthLogAction(29);

            betterHealthLogAction = BetterHealthLog;
            betterHealthLogAction("Texti", 18);

            Stack stacky = new Stack();
            stacky.Push(logAction);

            //Ziemlich cooles Zeug mit Actions:
            Action<int> bumAction = HealthLog;
            TestAction(bumAction);
        }
        
        private void TestAction(Action<int> a)
        {
            a(8);
        }
    }
}