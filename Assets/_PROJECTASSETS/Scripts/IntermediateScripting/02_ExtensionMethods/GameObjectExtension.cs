using UnityEngine;

namespace PGaS
{
    public static class GameObjectExtension {
        public static bool HasSameTag(this GameObject myselfGo, GameObject otherGo)
        {
            if(myselfGo.CompareTag(otherGo.tag)){
                return true;
            }
            return false;
        }
    }
}