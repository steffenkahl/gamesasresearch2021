﻿using UnityEngine;

namespace PGaS.IntermediateScripting.ExtensionMethods
{
    public class ExtensionMethodExample : MonoBehaviour
    {
        [SerializeField] private GameObject go;
        [SerializeField] private GameObject otherGo;

        private void Update()
        {
            Debug.Log(go.HasSameTag(otherGo));
        }
    }
}