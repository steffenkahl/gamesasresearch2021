﻿using PGaS.Events;
using UnityEngine;
using TMPro;

namespace PGaS
{
    public class PlayerHealth : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI textMesh;
        
        private void OnEnable()
        {
            Message<PlayerDamageEvent>.Add(OnPlayerDamageEvent);
        }
        
        private void OnDisable()
        {
            Message<PlayerDamageEvent>.Remove(OnPlayerDamageEvent);
        }
        
        private void OnPlayerDamageEvent(PlayerDamageEvent playerDamageEvent)
        {
            textMesh.text = "Player " + playerDamageEvent.PlayerGO.name + " got damaged by " + playerDamageEvent.DamageAmount + "HP";
        }
    }
}