﻿using System;
using PGaS.Events;
using UnityEngine;

namespace PGaS
{
    public class PlayerEventExample : MonoBehaviour
    {
        [SerializeField] private int health;
        [SerializeField] private string name;

        private void OnEnable()
        {
            Message<PlayerDamageEvent>.Add(OnPlayerDamageEvent);
        }
        
        private void OnDisable()
        {
            Message<PlayerDamageEvent>.Remove(OnPlayerDamageEvent);
        }
        
        private void OnPlayerDamageEvent(PlayerDamageEvent playerDamageEvent)
        {
            Debug.Log("Player " + playerDamageEvent.PlayerGO.name + " got damaged by " + playerDamageEvent.DamageAmount + "HP");
            health -= playerDamageEvent.DamageAmount;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            Message.Raise(new PlayerDamageEvent(gameObject, 10));
        }
    }
}