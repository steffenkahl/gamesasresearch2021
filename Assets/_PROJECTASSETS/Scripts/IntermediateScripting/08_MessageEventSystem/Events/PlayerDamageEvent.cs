﻿using UnityEngine;

namespace PGaS.Events
{
    public class PlayerDamageEvent
    {
        private int damageAmount;
        private GameObject playerGO;

        public int DamageAmount
        {
            get => damageAmount;
            set => damageAmount = value;
        }

        public GameObject PlayerGO
        {
            get => playerGO;
            set => playerGO = value;
        }

        public PlayerDamageEvent()
        { }

        public PlayerDamageEvent(GameObject playerGO, int damageAmount)
        {
            this.damageAmount = damageAmount;
            this.playerGO = playerGO;
        }
    }
}