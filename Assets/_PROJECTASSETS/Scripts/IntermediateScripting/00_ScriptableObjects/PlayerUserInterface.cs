using System;
using TMPro;
using Unity;
using UnityEngine;

namespace PGaS
{
    public class PlayerUserInterface : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI healthTextMesh;
        [SerializeField] private TextMeshProUGUI staminaTextMesh;
        [SerializeField] private TextMeshProUGUI speedTextMesh;
        [SerializeField] private EntityData playerData;

        private void Update()
        {
            healthTextMesh.text = playerData.Health.ToString();
            staminaTextMesh.text = playerData.Stamina.ToString();
            speedTextMesh.text = playerData.Speed.ToString();
        }
    }
}