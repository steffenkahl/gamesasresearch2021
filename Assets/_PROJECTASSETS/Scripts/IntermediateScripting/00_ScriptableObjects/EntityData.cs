using UnityEngine;

namespace PGaS
{
    [CreateAssetMenu(fileName = "EntityData", menuName = "PGameAsResearch/ScriptableObjects/Create Entity Data", order = 1)]
    public class EntityData : ScriptableObject
    {
        [SerializeField]
        private int health;

        [SerializeField]
        private int stamina;

        [SerializeField]
        private float speed;

        public int Health
        {
            get { return health; }
            set { health = value; }
        }

        public int Stamina
        {
            get => stamina;
            set => stamina = value;
        }

        public float Speed
        {
            get => speed;
            set => speed = value;
        }
    }
}