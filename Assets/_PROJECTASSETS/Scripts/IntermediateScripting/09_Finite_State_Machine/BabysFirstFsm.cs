﻿using System;
using FSM;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting._09_Finite_State_Machine
{
    public class BabysFirstFsm : MonoBehaviour
    {
        [SerializeField] private int speed;
        [SerializeField] private string[] states;
        
        private StateMachine fsm;

        private void Start()
        {
            fsm = new StateMachine();

            for (int i = 0; i < states.Length; i++)
            {
                fsm.AddState(states[i]);
            }
            
            fsm.AddState("IDLE", OnEnterIdle,OnLogicIdle, OnExitIdle);
            fsm.AddState("WALK");
            
            fsm.AddTransition("IDLE", "WALK", FromIdleToWalk);
            
            fsm.SetStartState("IDLE");
            fsm.Init();
        }

        private bool FromIdleToWalk(Transition<string> arg)
        {
            if (speed >= 5)
            {
                Debug.Log("Transition successful");
            }
            
            return speed >= 5;
        }

        private void OnEnterIdle(State<string> obj)
        {
            Debug.Log("Idle state entered");
        }

        private void OnLogicIdle(State<string> obj)
        {
            Debug.Log("Idle is idling");
        }

        private void OnExitIdle(State<string> obj)
        {
            Debug.Log("Exited Idle");
        }

        private void Update()
        {
            fsm.OnLogic();
        }
    }
    
    public enum State{
        IDLE,
        WALK
    }
}