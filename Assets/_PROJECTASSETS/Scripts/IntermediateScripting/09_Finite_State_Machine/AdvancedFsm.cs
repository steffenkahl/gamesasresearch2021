﻿using System;
using System.Collections;
using FSM;
using UnityEngine;

namespace _PROJECTASSETS.Scripts.IntermediateScripting._09_Finite_State_Machine
{
    public class AdvancedFsm : MonoBehaviour
    {
        [SerializeField] private State[] states;
        [SerializeField] private State startState;
        [SerializeField] private int speed;
        private StateMachine fsm;

        private void Start()
        {
            for (int i = 0; i < states.Length; i++)
            {
                fsm.AddState(states[i].ToString(), onLogic: OnLogicState);
            }
            
            fsm.SetStartState(startState.ToString());
            fsm.Init();
        }

        private void OnLogicState(State<string> obj)
        {
            if (obj.fsm.ActiveStateName == State.IDLE.ToString())
            {
                Debug.Log("Bin im Idle");
            }
            
            if (obj.fsm.ActiveStateName == State.WALK.ToString())
            {
                Debug.Log("Bin im Walk");
            }
        }
        
        private bool FromIdleToWalk(Transition<string> arg)
        {
            if (speed >= 5)
            {
                Debug.Log("Transition successful");
            }
            
            return speed >= 5;
        }

        private void Update()
        {
            fsm.OnLogic();
        }
    }
}