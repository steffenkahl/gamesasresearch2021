﻿using System;
using UnityEngine;

namespace PGaS
{
    public class ChangeColorBehaviour : MonoBehaviour
    {
        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        private void OnEnable()
        {
            EventManager.onInputEvent += OnInputCallback;
        }
        
        private void OnDisable()
        {
            EventManager.onInputEvent -= OnInputCallback;
        }

        private void OnInputCallback()
        {
            spriteRenderer.color = Color.blue;
        }
    }
}