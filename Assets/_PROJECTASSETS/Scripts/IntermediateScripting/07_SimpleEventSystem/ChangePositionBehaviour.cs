﻿using System;
using UnityEngine;

namespace PGaS
{
    public class ChangePositionBehaviour : MonoBehaviour
    {
        [SerializeField] private Vector3 newPosition;

        private void OnEnable()
        {
            EventManager.onStartEvent += OnStartCallback;
        }

        private void OnDisable()
        {
            EventManager.onStartEvent -= OnStartCallback;
        }

        private void OnStartCallback()
        {
            transform.position = newPosition;
        }
    }
}