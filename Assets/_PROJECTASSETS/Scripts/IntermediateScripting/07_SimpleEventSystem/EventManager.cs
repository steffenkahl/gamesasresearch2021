using UnityEngine;

namespace PGaS
{
    public class EventManager : MonoBehaviour
    {
        public delegate void OnInput();
        
        public delegate void OnStart();

        public static event OnInput onInputEvent;
        public static event OnStart onStartEvent;

        public static void RaiseOnInput()
        {
            if (onInputEvent != null)
            {
                onInputEvent();
            }
        }

        public static void RaiseOnStart()
        {
            if (onStartEvent != null)
            {
                onStartEvent();
            }
        }
    }
}