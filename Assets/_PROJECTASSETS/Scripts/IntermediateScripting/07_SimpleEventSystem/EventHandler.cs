﻿using System;
using UnityEngine;

namespace PGaS
{
    public class EventHandler : MonoBehaviour
    {
        private void Start()
        {
            EventManager.RaiseOnStart();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.A))
            {
                EventManager.RaiseOnInput();
            }
        }
    }
}